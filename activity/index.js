// item 1


let getCube = (number) => {
   console.log(`The Cube of ${number} is ${number ** 3} `);
}
getCube(2);

// item 2

let address={
  houseNumber: 258,
  streetNumber: "Washington Ave NW",
  cityDetail: "California",
  zipCode:90011
}

let {houseNumber, streetNumber, cityDetail,zipCode} = address
let displayAddress = (addressHouseNumber,addressStreetNumber,addressCityDetail,addressZipCode) =>{
  console.log(`I live at ${houseNumber} ${streetNumber} ${cityDetail} ${zipCode}` );
}
displayAddress(houseNumber,streetNumber,cityDetail,zipCode);

// item 3

let animal={
  animalName: "Awi",
  animalType: "Dog",
  animalBreed: "Maltesian",
  animalAge: 3
}

let {animalName,animalType,animalBreed,animalAge}=animal;
let displayAnimalDetails =(animalDetailName,animalDetailType,animalDetailBreed,animalDetailAge)=>{
  console.log(`${animalDetailName} is my pet, she is a ${animalDetailType} ${animalDetailAge} years old with a ${animalDetailBreed} breed`);
}
displayAnimalDetails(animalName,animalType,animalBreed,animalAge);

// item 4

let numbers =[1,2,3,4,5,15];

numbers.forEach((numberDetails) => {
	console.log(`${numberDetails}`);
});

// item 5

class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog= new Dog("Awi", 3, "Maltese");
console.log(myDog);
